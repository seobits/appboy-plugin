#import "PluginAppboy.h"
#import "AppboyKit.h"

#include <CoronaRuntime.h>
#include <CoronaLuaIOS.h>
#import <UIKit/UIKit.h>

// ----------------------------------------------------------------------------

class PluginAppboy
{
	public:
		typedef PluginAppboy Self;

	public:
		static const char kName[];
		static const char kEvent[];

	protected:
		PluginAppboy();

	public:
		bool Initialize( CoronaLuaRef listener );

	public:
		CoronaLuaRef GetListener() const { return fListener; }

	public:
		static int Open( lua_State *L );

	protected:
		static int Finalizer( lua_State *L );

	public:
		static Self *ToLibrary( lua_State *L );

	public:
		static int init( lua_State *L );
        static int getDeviceId(lua_State *L);
        static int changeUser(lua_State *L);
        static int flushDataAndProcessRequestQueue(lua_State *L);
        static int logCustomEvent(lua_State *L);
        static int logPurchase(lua_State *L);
        static int registerPushToken(lua_State *L);
        static int setUserInfo(lua_State *L);
        static int setCustomAttribute(lua_State *L);
	private:
		CoronaLuaRef fListener;
};

// ----------------------------------------------------------------------------

// This corresponds to the name of the library, e.g. [Lua] require "plugin.library"
const char PluginAppboy::kName[] = "plugin.appboy";

// This corresponds to the event name, e.g. [Lua] event.name
const char PluginAppboy::kEvent[] = "pluginappboy";

PluginAppboy::PluginAppboy()
:	fListener( NULL )
{
}

bool
PluginAppboy::Initialize( CoronaLuaRef listener )
{
	// Can only initialize listener once
	bool result = ( NULL == fListener );

	if ( result )
	{
		fListener = listener;
	}

	return result;
}

int
PluginAppboy::Open( lua_State *L )
{
	// Register __gc callback
	const char kMetatableName[] = __FILE__; // Globally unique string to prevent collision
	CoronaLuaInitializeGCMetatable( L, kMetatableName, Finalizer );

	// Functions in library
	const luaL_Reg kVTable[] =
	{
        {"init", init},
        {"getDeviceId", getDeviceId},
        {"changeUser", changeUser},
        {"flushDataAndProcessRequestQueue", flushDataAndProcessRequestQueue},
        {"logCustomEvent", logCustomEvent},
        {"logPurchase", logPurchase},
        {"registerPushToken", registerPushToken},
        {"setUserInfo", setUserInfo},
        {"setCustomAttribute", setCustomAttribute},
		{ NULL, NULL }
	};

	// Set library as upvalue for each library function
	Self *library = new Self;
	CoronaLuaPushUserdata( L, library, kMetatableName );

	luaL_openlib( L, kName, kVTable, 1 ); // leave "library" on top of stack

	return 1;
}

int
PluginAppboy::Finalizer( lua_State *L )
{
	Self *library = (Self *)CoronaLuaToUserdata( L, 1 );

	CoronaLuaDeleteRef( L, library->GetListener() );

	delete library;

	return 0;
}

PluginAppboy *
PluginAppboy::ToLibrary( lua_State *L )
{
	// library is pushed as part of the closure
	Self *library = (Self *)CoronaLuaToUserdata( L, lua_upvalueindex( 1 ) );
	return library;
}

// [Lua] library.init( listener )
int
PluginAppboy::init( lua_State *L )
{
    int nArg = lua_gettop(L);
    if(nArg >= 1){
        NSDictionary *launchoptions = nil;
        NSString *apiKey = [NSString stringWithUTF8String:luaL_checkstring(L, 1)];
    
        [Appboy startWithApiKey:apiKey
                inApplication: UIApplication.sharedApplication
                withLaunchOptions: launchoptions];
    }else{
        luaL_error(L, "You need to provide a key no initialize appboy");
    }
    
    
	return 0;
}

int
PluginAppboy::getDeviceId(lua_State *L){
    
    NSString *deviceId = [[Appboy sharedInstance] getDeviceId];
    const char *cString = [deviceId cStringUsingEncoding:NSUTF8StringEncoding];
    
    lua_pushstring(L, cString);
    return 1;
}

int
PluginAppboy::setUserInfo(lua_State *L){
    
    int nArg = lua_gettop(L);
    if(nArg >= 1){
        if(lua_istable(L, 1)){
            NSDictionary *properties = CoronaLuaCreateDictionary(L, 1);
            NSDate *dateOfBirth;
            for(NSString *propKey in properties){
                NSString *propValue = [properties objectForKey:propKey];
                if([propKey isEqualToString:@"dateOfBirth"]){
                    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
                    [dateFormater setDateFormat:@"MM-dd-yyyy"];
                    dateOfBirth = [dateFormater dateFromString:propValue];
                    [[Appboy sharedInstance].user setDateOfBirth:dateOfBirth];
                }
                
                if([propKey isEqualToString:@"gender"]){
                    ABKUserGenderType genderType = ABKUserGenderFemale;
                    if([propValue isEqualToString:@"male"]){
                        genderType = ABKUserGenderMale;
                    }
                    [[Appboy sharedInstance].user setGender:genderType];
                }
            }
            
            [[Appboy sharedInstance].user setValuesForKeysWithDictionary:properties];
        }
    }
    return 0;
}

int
PluginAppboy::changeUser(lua_State *L){
    
    int nArg = lua_gettop(L);
    if(nArg >= 1){
        NSLog(@"[Appboy Plugin]: Setting user info");
        NSString *userId = [NSString stringWithUTF8String:luaL_checkstring(L, 1)];
        [[Appboy sharedInstance] changeUser:userId];
    }
    return 0;
}

int
PluginAppboy::flushDataAndProcessRequestQueue(lua_State *L){
    [[Appboy sharedInstance] flushDataAndProcessRequestQueue];
    return 0;
}

int
PluginAppboy::logCustomEvent(lua_State *L){
    int nArg = lua_gettop(L);
    if(nArg >= 1){
        NSString *customEvent = [NSString stringWithUTF8String:luaL_checkstring(L, 1)];
        NSDictionary *properties = @{};
        if(nArg >= 2){
            if(lua_istable(L, 2)){
                properties = CoronaLuaCreateDictionary(L, 2);
            }
        }
        [[Appboy sharedInstance] logCustomEvent:customEvent withProperties:properties];
    }
    return 0;
}

int PluginAppboy::logPurchase(lua_State *L){
    int nArg = lua_gettop(L);
    if(nArg >= 1){
        NSDictionary *purchaseProperties = CoronaLuaCreateDictionary(L, 1);
        NSString *name = [purchaseProperties objectForKey:@"name"];
        NSString *currency = [purchaseProperties objectForKey:@"currency"];
        NSNumber *price = [purchaseProperties objectForKey:@"price"];
        NSNumber *quantity = [purchaseProperties objectForKey:@"quantity"];
        NSDictionary *properties = [purchaseProperties objectForKey:@"properties"];
        
        bool canLogEvent = name != nil && currency != nil && price != nil;
        
        if(canLogEvent){
            if(canLogEvent && quantity != nil){
                [[Appboy sharedInstance]
                 logPurchase:name
                 inCurrency:currency
                 atPrice: [NSDecimalNumber decimalNumberWithDecimal:[price decimalValue]]
                 withQuantity: [quantity unsignedIntegerValue]
                 andProperties: properties
                 ];
                return 0;
            }
            
            [[Appboy sharedInstance]
                logPurchase:name
                inCurrency:currency
                atPrice: [NSDecimalNumber decimalNumberWithDecimal:[price decimalValue]]
                withProperties:properties
             ];
        }
    }
    
    return 0;
}

int PluginAppboy::setCustomAttribute(lua_State *L){
    if(lua_istable(L, 1)){
        Appboy *appboyInstance = [Appboy sharedInstance];
        NSDictionary * attributes = CoronaLuaCreateDictionary(L, 1);
        for(id key in attributes){
            NSObject *currentValue = attributes[key];
            if([currentValue isKindOfClass:[NSString class]]){
                [appboyInstance.user setCustomAttributeWithKey: key andStringValue: (NSString *)currentValue];
            }else if([currentValue isKindOfClass:[NSNumber class]]){
                NSNumber * number = (NSNumber *) currentValue;
                CFNumberType numberType = CFNumberGetType((CFNumberRef) number);
                switch(numberType){
                    case kCFNumberSInt8Type:
                    case kCFNumberSInt16Type:
                    case kCFNumberSInt32Type:
                    case kCFNumberSInt64Type:
                    case kCFNumberCharType:
                    case kCFNumberShortType:
                    case kCFNumberIntType:
                    case kCFNumberCFIndexType:
                    case kCFNumberNSIntegerType:
                        [appboyInstance.user setCustomAttributeWithKey: key andIntegerValue: [number integerValue]];
                        break;
                        
                    case kCFNumberFloat32Type:
                    case kCFNumberFloat64Type:
                    case kCFNumberLongType:
                    case kCFNumberLongLongType:
                    case kCFNumberFloatType:
                    case kCFNumberDoubleType:
                    case kCFNumberCGFloatType:
                        [appboyInstance.user setCustomAttributeWithKey: key andDoubleValue: [number doubleValue]];
                        break;
                }
                
            }
        }
        
    }
    return 0;
}

int PluginAppboy::registerPushToken(lua_State *L){
    int nArg = lua_gettop(L);
    if(nArg > 1){
        NSString *token = [NSString stringWithUTF8String:luaL_checkstring(L, 1)];
        [[Appboy sharedInstance] registerPushToken:token];
    }
    return 0;
}

// ----------------------------------------------------------------------------

CORONA_EXPORT int luaopen_plugin_appboy( lua_State *L )
{
	return PluginAppboy::Open( L );
}
