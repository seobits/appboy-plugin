#ifndef _PluginAppboy_H__
#define _PluginAppboy_H__

#include <CoronaLua.h>
#include <CoronaMacros.h>

CORONA_EXPORT int luaopen_plugin_appboy( lua_State *L );

#endif
