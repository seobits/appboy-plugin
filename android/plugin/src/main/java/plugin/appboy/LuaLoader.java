//
//  LuaLoader.java
//  TemplateApp
//
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

// This corresponds to the name of the Lua library,
// e.g. [Lua] require "plugin.library"
package plugin.appboy;

import com.ansca.corona.CoronaActivity;
import com.ansca.corona.CoronaData;
import com.ansca.corona.CoronaEnvironment;
import com.ansca.corona.CoronaLua;
import com.ansca.corona.CoronaRuntime;
import com.ansca.corona.CoronaRuntimeListener;
import com.ansca.corona.CoronaRuntimeTask;
import com.ansca.corona.CoronaRuntimeTaskDispatcher;
import com.ansca.corona.ApplicationContextProvider;
import com.appboy.models.outgoing.AppboyProperties;
import com.naef.jnlua.JavaFunction;
import com.naef.jnlua.LuaState;
import com.naef.jnlua.NamedJavaFunction;
import android.content.Context;
import android.util.Log;

import com.appboy.configuration.*;
import com.appboy.Appboy;
import com.appboy.AppboyUser;
import com.appboy.enums.Gender;

import org.json.JSONObject;

import java.math.MathContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.Set;
import java.util.Set;
import java.util.Iterator;

/*
 * Implements the Lua interface for a Corona plugin.
 * <p>
 * Only one instance of this class will be created by Corona for the lifetime of the application.
 * This instance will be re-used for every new Corona activity that gets created.
 */
@SuppressWarnings("WeakerAccess")
public class LuaLoader implements JavaFunction, CoronaRuntimeListener {
	/** Lua registry ID to the Lua function to be called when the ad request finishes. */
	private int fListener;

	/** This corresponds to the event name, e.g. [Lua] event.name */
	private static final String EVENT_NAME = "appboyevent";


	/**
	 * Creates a new Lua interface to this plugin.
	 * <p>
	 * Note that a new LuaLoader instance will not be created for every CoronaActivity instance.
	 * That is, only one instance of this class will be created for the lifetime of the application process.
	 * This gives a plugin the option to do operations in the background while the CoronaActivity is destroyed.
	 */
	@SuppressWarnings("unused")
	public LuaLoader() {
		// Initialize member variables.
		fListener = CoronaLua.REFNIL;

		// Set up this plugin to listen for Corona runtime events to be received by methods
		// onLoaded(), onStarted(), onSuspended(), onResumed(), and onExiting().
		CoronaEnvironment.addRuntimeListener(this);
	}

	/**
	 * Called when this plugin is being loaded via the Lua require() function.
	 * <p>
	 * Note that this method will be called every time a new CoronaActivity has been launched.
	 * This means that you'll need to re-initialize this plugin here.
	 * <p>
	 * Warning! This method is not called on the main UI thread.
	 * @param L Reference to the Lua state that the require() function was called from.
	 * @return Returns the number of values that the require() function will return.
	 *         <p>
	 *         Expected to return 1, the library that the require() function is loading.
	 */
	@Override
	public int invoke(LuaState L) {
		// Register this plugin into Lua with the following functions.
		NamedJavaFunction[] luaFunctions = new NamedJavaFunction[] {
			    new InitWrapper(),
			    new GetDeviceIdWrapper(),
                new ChangeUserWrapper(),
                new LogCustomEventWrapper(),
                new LogPurchaseWrapper(),
                new DataFlushWrapper(),
                new RegisterPushTokenWrapper(),
                new SetUserInfo(),
                new SetCustomAttribute(),
		};
		String libName = L.toString( 1 );
		L.register(libName, luaFunctions);

		// Returning 1 indicates that the Lua require() function will return the above Lua library.
		return 1;
	}

	/**
	 * Called after the Corona runtime has been created and just before executing the "main.lua" file.
	 * <p>
	 * Warning! This method is not called on the main thread.
	 * @param runtime Reference to the CoronaRuntime object that has just been loaded/initialized.
	 *                Provides a LuaState object that allows the application to extend the Lua API.
	 */
	@Override
	public void onLoaded(CoronaRuntime runtime) {
		// Note that this method will not be called the first time a Corona activity has been launched.
		// This is because this listener cannot be added to the CoronaEnvironment until after
		// this plugin has been required-in by Lua, which occurs after the onLoaded() event.
		// However, this method will be called when a 2nd Corona activity has been created.

	}

	/**
	 * Called just after the Corona runtime has executed the "main.lua" file.
	 * <p>
	 * Warning! This method is not called on the main thread.
	 * @param runtime Reference to the CoronaRuntime object that has just been started.
	 */
	@Override
	public void onStarted(CoronaRuntime runtime) {
	}

	/**
	 * Called just after the Corona runtime has been suspended which pauses all rendering, audio, timers,
	 * and other Corona related operations. This can happen when another Android activity (ie: window) has
	 * been displayed, when the screen has been powered off, or when the screen lock is shown.
	 * <p>
	 * Warning! This method is not called on the main thread.
	 * @param runtime Reference to the CoronaRuntime object that has just been suspended.
	 */
	@Override
	public void onSuspended(CoronaRuntime runtime) {
	}

	/**
	 * Called just after the Corona runtime has been resumed after a suspend.
	 * <p>
	 * Warning! This method is not called on the main thread.
	 * @param runtime Reference to the CoronaRuntime object that has just been resumed.
	 */
	@Override
	public void onResumed(CoronaRuntime runtime) {
	}

	/**
	 * Called just before the Corona runtime terminates.
	 * <p>
	 * This happens when the Corona activity is being destroyed which happens when the user presses the Back button
	 * on the activity, when the native.requestExit() method is called in Lua, or when the activity's finish()
	 * method is called. This does not mean that the application is exiting.
	 * <p>
	 * Warning! This method is not called on the main thread.
	 * @param runtime Reference to the CoronaRuntime object that is being terminated.
	 */
	@Override
	public void onExiting(CoronaRuntime runtime) {

		// Remove the Lua listener reference.
		CoronaLua.deleteRef( runtime.getLuaState(), fListener );
		fListener = CoronaLua.REFNIL;

	}

	/**
	 * The following Lua function has been called:  library.init( listener )
	 * <p>
	 * Warning! This method is not called on the main thread.
	 * @param L Reference to the Lua state that the Lua function was called from.
	 * @return Returns the number of values to be returned by the library.init() function.
	 */
	@SuppressWarnings({"WeakerAccess", "SameReturnValue"})
	public int init(LuaState L) {
        int argC = L.getTop();
        if(argC >= 1) {
            String apiKey = L.checkString(1);
            AppboyConfig.Builder builder = new AppboyConfig.Builder();
            builder.setApiKey(apiKey);

            Context appContext = ApplicationContextProvider.getApplicationContext();

            Appboy.configure(appContext, builder.build());
            Appboy appboyInstance = Appboy.getInstance(appContext);
            appboyInstance.openSession(CoronaEnvironment.getCoronaActivity());
        }
		return 0;
	}

	public int getDeviceId(LuaState L){
        int argC = L.getTop();

        Context appContext = ApplicationContextProvider.getApplicationContext();
        Appboy appboyInstance = Appboy.getInstance(appContext);
        String deviceId = appboyInstance.getDeviceId();
        L.pushString(deviceId);

        return 1;
    }

    public int setUserInfo(LuaState L) {
        int argC = L.getTop();
        if(argC >= 1){
            if(L.isTable(1)){
                Context appContext = ApplicationContextProvider.getApplicationContext();
                Appboy appboyInstance = Appboy.getInstance(appContext);
                AppboyUser user = appboyInstance.getCurrentUser();
                Hashtable propertiesHash = CoronaLua.toHashtable(L, 1);
                //This a very ugly way to do this but it seems there's no other way :'(
                //It might be posible with reflection, everything is mapped into
                //functions and the properties have private access.
                if(propertiesHash.containsKey("firstName")) {
                    user.setFirstName(propertiesHash.get("firstName").toString());
                }

                if(propertiesHash.containsKey("lastName")) {
                    user.setLastName(propertiesHash.get("lastName").toString());
                }

                if(propertiesHash.containsKey("gender")) {
                    String genderString = propertiesHash.get("gender").toString();
                    Gender gender = Gender.FEMALE;
                    if(genderString.equals("male")){
                        gender = Gender.MALE;
                    }
                    user.setGender(gender);
                }

                if(propertiesHash.containsKey("email")) {
                    user.setEmail(propertiesHash.get("email").toString());
                }

                if(propertiesHash.containsKey("dateOfBirth")) {
                    String dateString = propertiesHash.get("dateOfBirth").toString();
                    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
                    Date date;
                    try{
                        date = dateFormat.parse(dateString);
                    }catch(ParseException e){
                        return 0;
                    }
                    Calendar calendar = new GregorianCalendar();
                    calendar.setTime(date);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);
                    int month = calendar.get(Calendar.MONTH);
                    int year = calendar.get(Calendar.YEAR);

                    com.appboy.enums.Month appboyMonth = com.appboy.enums.Month.getMonth(month);
                    user.setDateOfBirth(year, appboyMonth, day);
                }

                if(propertiesHash.containsKey("country")) {
                    user.setCountry(propertiesHash.get("country").toString());
                }

                if(propertiesHash.containsKey("homeCity")) {
                    user.setHomeCity(propertiesHash.get("homeCity").toString());
                }

                if(propertiesHash.containsKey("language")) {
                    user.setLanguage(propertiesHash.get("language").toString());
                }

                if(propertiesHash.containsKey("phone")) {
                    user.setPhoneNumber(propertiesHash.get("phone").toString());
                }

                if(propertiesHash.containsKey("avatarImageURL")) {
                    user.setAvatarImageUrl(propertiesHash.get("avatarImageURL").toString());
                }

            }
        }
        return 0;
    }

    public int changeUser(LuaState L) {
        int argC = L.getTop();
        if(argC >= 1){
            String user = L.checkString(1);
            Context appContext = ApplicationContextProvider.getApplicationContext();
            Appboy appboyInstance = Appboy.getInstance(appContext);
            appboyInstance.changeUser(user);
        }
        return 0;
    }

    public int logCustomEvent(LuaState L) {
        int argC = L.getTop();
        if(argC >= 1){
            String eventName = L.checkString(1);
            Context appContext = ApplicationContextProvider.getApplicationContext();
            Appboy appboyInstance = Appboy.getInstance(appContext);
            if(argC >= 2) {
                if(L.isTable(2)) {
                    Hashtable propertiesHash = CoronaLua.toHashtable(L, 2);
                    java.util.Map<String, String> pMap = propertiesHash;

                    JSONObject jsonProperties = new JSONObject(pMap);

                    AppboyProperties properties = new AppboyProperties(jsonProperties);
                    appboyInstance.logCustomEvent(eventName, properties);
                    return 0;
                }
            }
            appboyInstance.logCustomEvent(eventName);
        }
        return 0;
    }

    public int logPurchase(LuaState L){
        int argC = L.getTop();
        if(argC >= 1){
            if(L.isTable(1)){
                Hashtable propertiesTable = CoronaLua.toHashtable(L, 1);
                Object name = propertiesTable.get("name");
                Object currency = propertiesTable.get("currency");
                Object price = propertiesTable.get("price");
                Object quantity = propertiesTable.get("quantity");
                Object properties = propertiesTable.get("properties");

                AppboyProperties appboyProperties = new AppboyProperties();
                if(properties != null){
                    java.util.Map<String, String> pMap = (Hashtable)properties;
                    JSONObject jsonProperties = new JSONObject(pMap);
                    appboyProperties = new AppboyProperties(jsonProperties);
                }

                Boolean canLogEvent = name != null && currency != null && price != null;

                if(canLogEvent){
                    Context appContext = ApplicationContextProvider.getApplicationContext();
                    Appboy appboyInstance = Appboy.getInstance(appContext);
                    BigDecimal convertedPrice = new BigDecimal((Double)price, MathContext.DECIMAL64);
                    if(quantity != null){
                        appboyInstance.logPurchase( (String)name, (String)currency, convertedPrice, (int)quantity, appboyProperties);
                        return 0;
                    }
                    appboyInstance.logPurchase( (String)name, (String)currency, convertedPrice, appboyProperties);
                }


            }

        }
        return 0;
    }

    public int flushDataAndProcessRequestQueue(LuaState L){
        Context appContext = ApplicationContextProvider.getApplicationContext();
        Appboy appboyInstance = Appboy.getInstance(appContext);
        appboyInstance.requestImmediateDataFlush();
        return 0;
    }

    public int setCustomAttribute(LuaState L){

        if(L.isTable(1)){
            Context appContext = ApplicationContextProvider.getApplicationContext();
            Appboy appboyInstance = Appboy.getInstance(appContext);
            AppboyUser user = appboyInstance.getCurrentUser();
            Hashtable hashtable = CoronaLua.toHashtable(L, 1);

            Set<Object> tableKeys = hashtable.keySet();
            Iterator<Object> tableIterator = tableKeys.iterator();
            while(tableIterator.hasNext()){
                Object currentKey = tableIterator.next();
                Object currentObject = hashtable.get(currentKey);
                if(currentObject instanceof Double){
                    user.setCustomUserAttribute(currentKey.toString(), (Double) currentObject);
                    Log.v("APPBOY", currentObject.toString());
                }else if(currentObject instanceof java.lang.Boolean){
                    user.setCustomUserAttribute(currentKey.toString(), (Boolean) currentObject);
                }else if(currentObject instanceof java.lang.String){
                    user.setCustomUserAttribute(currentKey.toString(), (String) currentObject);
                }else if(currentObject instanceof java.lang.Integer){
                    user.setCustomUserAttribute(currentKey.toString(), (Integer) currentObject);
                }else if(currentObject instanceof java.lang.Long){
                    user.setCustomUserAttribute(currentKey.toString(), (Long) currentObject);
                }
            }
        }
        return 0;
    }

    public int registerPushToken(LuaState L){
        return 0;
    }

	/** Implements the library.init() Lua function. */
	@SuppressWarnings("unused")
	private class InitWrapper implements NamedJavaFunction {
        /**
         * Gets the name of the Lua function as it would appear in the Lua script.
         * @return Returns the name of the custom Lua function.
         */
        @Override
        public String getName() {
            return "init";
        }

        /**
         * This method is called when the Lua function is called.
         * <p>
         * Warning! This method is not called on the main UI thread.
         * @param L Reference to the Lua state.
         *                 Needed to retrieve the Lua function's parameters and to return values back to Lua.
         * @return Returns the number of values to be returned by the Lua function.
         */
        @Override
        public int invoke(LuaState L) {
            return init(L);
        }
    }

    private class GetDeviceIdWrapper implements NamedJavaFunction {
        public String getName() {
            return "getDeviceId";
        }
        public int invoke(LuaState L) {
            return getDeviceId(L);
        }
    }

    private class ChangeUserWrapper implements NamedJavaFunction {
        public String getName() {
            return "changeUser";
        }
        public int invoke(LuaState L) {
            return changeUser(L);
        }
    }

    private class LogCustomEventWrapper implements NamedJavaFunction {
        public String getName() {
            return "logCustomEvent";
        }
        public int invoke(LuaState L) {
            return logCustomEvent(L);
        }
    }

    private class LogPurchaseWrapper implements NamedJavaFunction {
        public String getName() {
            return "logPurchase";
        }
        public int invoke(LuaState L) {
            return logPurchase(L);
        }
    }

    private class DataFlushWrapper implements NamedJavaFunction {
        public String getName() {
            return "flushDataAndProcessRequestQueue";
        }
        public int invoke(LuaState L) {
            return flushDataAndProcessRequestQueue(L);
        }
    }

    private class RegisterPushTokenWrapper implements NamedJavaFunction {
        public String getName() {
            return "registerPushToken";
        }
        public int invoke(LuaState L) {
            return registerPushToken(L);
        }
    }

    private class SetUserInfo implements NamedJavaFunction {
        public String getName() {return "setUserInfo";}
        public int invoke(LuaState L) {
            return setUserInfo(L);
        }
    }

    private class SetCustomAttribute implements NamedJavaFunction {
        public String getName() {return "setCustomAttribute";}
        public int invoke(LuaState L) {
            return setCustomAttribute(L);
        }
    }
}
